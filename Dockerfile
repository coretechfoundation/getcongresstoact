FROM centos:7

# Install dependencies
RUN yum install -y epel-release
RUN yum install -y httpd nodejs git curl

# Install app
RUN rm -rf /var/www/html/*
ADD src /var/www/html

# Configure apache
#RUN chown -R www-data:www-data /var/www
#ENV APACHE_RUN_USER www-data
#ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

EXPOSE 80

CMD ["/usr/sbin/httpd", "-D",  "FOREGROUND"]
